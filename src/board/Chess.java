package board;

import java.util.Scanner;

import pieces.Bishop;
import pieces.Knight;
import pieces.Pawn;
import pieces.Piece;
import pieces.Queen;
import pieces.Rook;

/**
 * @author Chenjie Wu, Chuanqi Xiong
 *
 */
public class Chess {

	public static void main(String[] args)
	{
		ChessBoard chess = new ChessBoard();
		chess.update();

		int end;
		int draw = 0;
		
		while(true)
		{
//			for (int i =0; i < 2; i++) {
//				for (int j =0; j < 16;j++)
//				{
//					pieceInfo(chess.pieces[i][j]);
//				}
//			}
			
			// print the board
			chess.printBoard();

			end = Action(chess,draw);

			// case1: checkmate
			if (end == -1)
				break;
			// case2: draw agreement
			if (end == 2 && draw == 1)
				break;
			// case3: stalemate
			if (end == 3) {
				break;
			}
			// case4: game continue || draw refusal
			if (end == 0)
				draw = 0;
			// case5: draw proposal
			if (end == 1)
				draw++;

			// print check in main?

			movetoken++;
			System.out.println("");
		}
	}

	/**
	 * move	counts the number of both sides' moves
	 */
	private static int movetoken = 0;

	/**
	 * determine which side should move for each Turn
	 */
	private static void whoseMove()
	{
		if (movetoken % 2 == 0)
			System.out.print("White's move: ");
		else
			System.out.print("Black's move: ");
	}

	
	/**
	 * print out who wins
	 */
	private static void whoseWin()
	{
		if (movetoken % 2 == 0)
			System.out.print("White wins");
		else
			System.out.print("Black wins");
	}


	// "check"
	// "Checkmate" or "Stalemate", if detected.
	// "Illegal move, try again"
	// last statement, "Black wins", "White wins" or "draw"
	// input draw twice, resign

	/**
	 * @param chess	a chessBoard object
	 * @param draw	draw type counter
	 * @return	-1	if one player wins(checkmate, resign)
	 * 			0	if game continue
	 * 			1	if draw proposed
	 * 			2	if draw is agreed
	 * 			3 	if stalemate
	 */
	private static int Action(ChessBoard chess, int draw)
	{
		while(true)
		{
			// print who's move
			whoseMove();

			// scan the input to the end of line
			Scanner input = new Scanner(System.in);
			String instruction = input.nextLine();

			int file,rank,newF,newR;
			char promochar = 'Q'; 		//default promotion



			// case0: input empty
			if (instruction == null || instruction.isEmpty())
			{
				System.out.println("Illegal move, try again");
				continue;
			}

			// case1: resign
			if (instruction.equals("resign")) {
				String s = (movetoken % 2 == 0? "Black wins":"White wins");
				System.out.println(s);
				return -1;
			}

			// case2: agree on oppo's draw
			else if (instruction.equals("draw")){
				/*implement draw agreement*/

				// no draw proposal
				if (draw != 1){
					System.out.println("Illegal move, try again");
					continue;
				}
				// draw agreement, game end
				else {
					System.out.println("draw");
					return 2;
				}
			}

			// input too short
			if (instruction.length() < 5)
			{
				System.out.println("Illegal move, try again");
				continue;
			}

			if (instruction.length() > 5)
			{
				// invalid promotion char
				if (instruction.length() == 7)
				{
					promochar = instruction.charAt(6);
					if (promochar != 'Q' && promochar != 'R' && promochar != 'B' && promochar != 'N')
					{
						System.out.println("Illegal move, try again");
						continue;
					}

					// end, iff promochar correct
				}
				// invalid draw proposal
				else if (instruction.length() ==  11)
				{
					if (!instruction.contains("draw?"))
					{
						System.out.println("Illegal move, try again");
						continue;
					}

					// end, iff draw?
				}
				// other invalid input with various string length
				else {
					System.out.println("Illegal move, try again");
					continue;
				}
			}

			// scan inputs, all should be in Range of [0,7]
			file = (int)instruction.charAt(0) - 97;
			rank = (int)instruction.charAt(1) - 49;
			newF = (int)instruction.charAt(3) - 97;
			newR = (int)instruction.charAt(4) - 49;

			// out of range, invalid input
			if (!inRange(file) || !inRange(rank) || !inRange(newF) || !inRange(newR) || instruction.charAt(2) != ' ')
			{
				System.out.println("Illegal move, try again");
				continue;
			}

			if (chess.board[rank][file] == null)
            {
                System.out.println("Illegal move, try again");
                continue;
            }


//            System.out.println("rk:"+rank+",fl:"+file+",newR:"+newR+",newF:"+newF+",mt:"+movetoken);
//            pieceInfo(chess.board[rank][file]);

            int selfSide = (chess.board[rank][file].getSide() == 'w' ? 0:1);
            int oppoSide = (selfSide == 0? 1:0);

			if (movetoken % 2 != selfSide)
            {
                System.out.println("Illegal move, try again");
                continue;
            }


			// case3: input is a legal move
			/**
			 * legalType	-1:	invalid move
			 * 				0:	legal move
			 * 				1: 	enpassant
			 * 				2: 	castling
			 */
			int legalType = chess.isLegalMove(chess.board[rank][file],newR,newF,chess.board,chess.pieces,movetoken);
//			System.out.println("legalT:"+legalType);
//			chess.printBoard();
			
			
			if (legalType != -1)
			{
				// case3.1.1: capture, except en passant
				if (chess.board[newR][newF] != null)
				{
					//
					for (int i = 0; i < 2;  i++){
						for (int j = 0; j < 16; j++){
							int fl = chess.pieces[i][j].getFile();
							int rk = chess.pieces[i][j].getRank();

							// newR,newF has a not Captured pieces. Capture!
							if (fl == newF && rk == newR && !chess.pieces[i][j].isCaptured()) {
								chess.pieces[i][j].beCaptured();
							}
						}
					}

				}
				// case3.1.2: capture, en passant
				else if ( legalType == 1)
				{
					if ( chess.board[rank][file] instanceof Pawn) {
						// opposite pawn
						if (chess.board[rank][newF] instanceof Pawn) {


							for (int i = 0; i < 2;  i++){
								for (int j = 0; j < 16; j++){
									int fl = chess.pieces[i][j].getFile();
									int rk = chess.pieces[i][j].getRank();

									// opposite pawn is at rank,newF. Capture!
									if (fl == newF && rk == rank && !chess.pieces[i][j].isCaptured())
									{
										chess.pieces[i][j].beCaptured();
//										pieceInfo(chess.pieces[i][j]);
									}
								}
							}

						}
					}
				}

				// move piece to newR newF
				// look for corresponding piece from the board, and move

                for (int i = 0; i < 2;  i++){
                    for (int j = 0; j < 16; j++){
                        int fl = chess.pieces[i][j].getFile();
                        int rk = chess.pieces[i][j].getRank();

                        if (fl == file && rk == rank && !chess.pieces[i][j].isCaptured())
                            chess.pieces[i][j].move(newR,newF,movetoken);
                    }
                }
                chess.update();
                
				// case3.2: move without capture
				// case3.2.1: promotion?
				if (legalType == 0 && chess.board[newR][newF] instanceof Pawn )
				{
//					System.out.println("into promotion check");
					// white pawn promotion
					if (selfSide == 0 && newR == 7)
					{
						for (int i = 0; i < 2; i++)
						{
							for (int j = 0; j < 16; j++)
							{
								int rk = chess.pieces[i][j].getRank();
								int fl = chess.pieces[i][j].getFile();

								if (rk == newR && fl == newF && !chess.pieces[i][j].isCaptured())
								{
//									pieceInfo(chess.pieces[i][j]);
									chess.pieces[i][j] = promotion(chess.pieces[i][j], promochar);
//									pieceInfo(chess.pieces[i][j]);
								}
							}
						}

					}
					// black pawn promotion
					if (selfSide == 1 && newR == 0)
					{
						for (int i = 0; i < 2 ; i++)
						{
							for (int j = 0; j < 16 ; j++) {
								int rk = chess.pieces[i][j].getRank();
								int fl = chess.pieces[i][j].getFile();

								if (rk == newR && fl == newF && !chess.pieces[i][j].isCaptured()) {
//									pieceInfo(chess.pieces[i][j]);
									chess.pieces[i][j] = promotion(chess.pieces[i][j], promochar);
//									pieceInfo(chess.pieces[i][j]);
								}
									
							}
						}
					}

				}

				chess.update();
				// case3.2.2: castling?
				if (legalType == 2)
				{
					// king left castling
					int rookFile = (newF == 2? 0:7);
					int RnewFile = (newF == 2? 3:5);

					for (int i = 0; i < 2;  i++){
						for (int j = 0; j < 16; j++){
							int fl = chess.pieces[i][j].getFile();
							int rk = chess.pieces[i][j].getRank();

							// Castling is Valid.
							// King has already moved.
							// move Rook
							if (fl == rookFile && rk == rank && !chess.pieces[i][j].isCaptured())
								chess.pieces[i][j].move(rank,RnewFile,movetoken);
						}
					}
				}

				// reset and update board
				chess.update();

				// case3.3: check?
				boolean inCheck = false;
				int kingR = chess.pieces[oppoSide][4].getRank();
				int kingF = chess.pieces[oppoSide][4].getFile();

				for (int i = 0; i < 16; i++)
				{
					if (chess.pieces[selfSide][i].isValid(kingR,kingF,chess.board) &&
							!chess.pieces[selfSide][i].isCaptured())
						inCheck = true;
				}

				// case3.4: checkmate? || stalemate?
				// count if there's a legal move for opposite pieces
				int count = 0;
				for (int i = 0; i < 16; i++)
				{
					for (int x = 0; x < 8; x++)
					{
						for (int y = 0; y < 8; y++)
						{
							if (chess.isLegalMove(chess.pieces[oppoSide][i], x, y, chess.board, chess.pieces, movetoken)!= -1)
								count++;
						}
					}
				}
				// check/checkmate
				if (inCheck)
				{
					// checkmate
					if (count == 0) {
						whoseWin();
						return -1;
					}
					// check
					System.out.println("check");
				}

				// stalemate
				else {
					if (count == 0){
						System.out.println("Stalemate");
						return 3;
					}
				}

				// case3.5: draw?
				if (instruction.contains("draw?"))
					return 1;
				else
					return 0;
			}

			System.out.println("Illegal move, try again");
		}
	}

	/**
	 * @param p			a piece object
	 * @param promChar	a character indicating which type of piece the pawn is changed into (default Queen)
	 * @return			a promoted piece
	 */
	private static Piece promotion(Piece p, char promChar){

		int pTempRank = p.getRank();
		int pTempFile = p.getFile();
		char pTempSide = p.getSide();

		if(promChar == 'N') {
			Piece newp = new Knight(pTempRank, pTempFile, pTempSide);
			newp.promToken(movetoken);
			return newp;
			
		}else if(promChar == 'Q'){
			Piece newp = new Queen(pTempRank, pTempFile, pTempSide);
			newp.promToken(movetoken);
			return newp;
			
		}else if(promChar == 'R') {
			Piece newp = new Rook(pTempRank, pTempFile, pTempSide);
			newp.promToken(movetoken);
			return newp;
			
		}else if(promChar == 'B') {
			Piece newp = new Bishop(pTempRank, pTempFile, pTempSide);
			newp.promToken(movetoken);
			return newp;
		}
//		System.out.println("unsuccessful prom");
		return null;
	}
	

	/**
	 * @param i	rank or file integer
	 * @return	false 	if out of range
	 * 			true 	if in range
	 */
	private static boolean inRange(int i)
	{
		if (i < 0 || i > 7)
			return false;
		else
			return true;
	}
	
	
	/**
	 * @param p	a piece object
	 */
	private static void pieceInfo(Piece p) {
		System.out.println("\t"+p);
		System.out.println("\t"+p.getRank()+"\tfile:"+p.getFile()+"\t"+p.getSide()+"\t"+p.getLastMove());
		System.out.println("\t isCaptured:"+p.isCaptured());
	}


}
