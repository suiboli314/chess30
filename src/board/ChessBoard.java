package board;

import pieces.Bishop;
import pieces.King;
import pieces.Knight;
import pieces.Pawn;
import pieces.Piece;
import pieces.Queen;
import pieces.Rook;

/**
 * @author Chenjie Wu, Chuanqi Xiong
 *
 */
public class ChessBoard {

	// files and ranks are int array of length 8 (0-7)

	/**
	 * Piece[][] pieces,
	 * 2D 2*16 array to keep track of all pieces' info
	 * pieces[0] indicates white pieces
	 */
	public Piece[][] pieces = new Piece[2][16];

	/**
	 * Piece[][] board, [rank][field] ([row][column])
	 * 2D 8*8 array board to store chess board info
	 * board will update based on Piece[2][16]
	 */
	public Piece[][] board = new Piece[8][8];
	

	/**
	 * Initiate ChessBoard--2x16 pieces
	 * Piece[0] is white's Pieces, Piece[1] is black's
	 * total pieces of a side, w/b: 16
	 * */
	public ChessBoard()
	{
		for (int i = 0; i < 2; i++) {
			int rows;
			char side;

			if(i == 0) {
				rows = 0;
				side = 'w';
			}
			else {
				rows = 7;
				side = 'b';
			}
			// eight pieces: 2Rooks, 2Knights, 2Bishops, 1King, 1Queen
			pieces[i][0] = new Rook(rows,0,side);
			pieces[i][1] = new Knight(rows,1,side);
			pieces[i][2] = new Bishop(rows,2,side);
			pieces[i][3] = new Queen(rows,3,side);
			pieces[i][4] = new King(rows,4,side);
			pieces[i][5] = new Bishop(rows,5,side);
			pieces[i][6] = new Knight(rows,6,side);
			pieces[i][7] = new Rook(rows,7,side);


			// eight pawns
			for(int j = 0; j < 8; j++){
				int pawnRow;
				if (rows ==0) {
					pawnRow = rows + 1;
				}else{
					pawnRow = rows - 1;
				}
				pieces[i][j+8] = new Pawn(pawnRow,j,side);
			}
		}

	}


	/**
	 * update board based on Piece[2][16]
	 * */
	public void update()
	{
		for (int rk = 0; rk<8 ; rk++) {
			for (int fl = 0; fl<8; fl++) {
				board[rk][fl] = null;
			}
		}

		for (int i = 0; i < 2;  i++){
			for (int j = 0; j < 16; j++){
				int fl = pieces[i][j].getFile();
				int rk = pieces[i][j].getRank();
				if(!pieces[i][j].isCaptured())
					board[rk][fl] = pieces[i][j];
			}
		}
	}


	/**
	 * @param p		a piece object
	 * @param newR	intention move's rank
	 * @param newF	intention move's file
	 * @param board	the 8*8 piece array
	 * @param pieces	the 2*16 piece array
	 * @param moveToken	turn's recording token
	 * @return	-1:	invalid move
	 * 			0:	legal move
	 * 			1: 	enpassant
	 * 			2: 	castling
	 */
	public int isLegalMove(Piece p, int newR, int newF, Piece[][] board, Piece[][] pieces, int moveToken)
	{
		int legal = -1;
//		System.out.println("welcome to isLegalMove()");


		if (!p.isValid(newR,newF,board) || p.isCaptured())
		{
//			System.out.println("invalid or caputred");
			return legal;
		}


		boolean isEnp= false;
		boolean isPawn = false;
		
		boolean obliqMove = true;
		// check for enpassant
		if (p instanceof Pawn)
		{
			isPawn = true;
//			System.out.println("islegalmove(),enpassant?");
//			System.out.println("into check for enpas");
			char selfSide = p.getSide();
			char oppoSide;
			if (selfSide == 'w') {
				oppoSide = 'b';

				for (int i = 8; i<16 ; i++) {
					// case 1:	 eat the pawn on left side
					if (p.getRank() == 4 && newR-p.getRank() == 1 && p.getFile()-newF == 1) {
						if (pieces[1][i].getRank() == 4 && pieces[1][i].getFile() == newF) {
							if (moveToken - pieces[1][i].getLastMove() == 1) {
								legal = 1;
//								isEnp = true;
							}
							else
								obliqMove = false;
							
						}
					}
					// case 2:	eat the pawn on right side
					if (p.getRank() == 4 && newR-p.getRank() == 1 && newF-p.getFile() == 1) {
						if (pieces[1][i].getRank() == 4 && pieces[1][i].getFile() == newF) {
							if (moveToken - pieces[1][i].getLastMove() == 1) {
								legal = 1;
//								isEnp = true;
							}
							else
								obliqMove = false;
							
						}
					}
				}

//				if (legal != 1)
//					System.out.println("not enpassant");

			}else{
				oppoSide = 'w';

				for (int i = 8; i<16 ; i++) {
					// case 1:	 eat the pawn on left side
					if (p.getRank() == 3 && p.getRank()-newR == 1 && p.getFile()-newF == 1) {
						if (pieces[0][i].getRank() == 3 && pieces[0][i].getFile() == newF) {
							if (moveToken - pieces[0][i].getLastMove() == 1) {
								legal = 1;
								isEnp = true;
							}
							else
								obliqMove = false;
							
						}
					}
					// case 2:	eat the pawn on right side
					if (p.getRank() == 4 && newR == p.getRank()+1 && newF-p.getFile() == 1) {
						if (pieces[0][i].getRank() == 4 && pieces[0][i].getFile() == newF) {
							if (moveToken - pieces[0][i].getLastMove() == 1) {
								legal = 1;
								isEnp = true;
							}
							else
								obliqMove = false;
							
						}
					}
				}

//				if (legal != 1)
//					System.out.println("not enpassant");
			}
		}

		// check for castling
		boolean castleFailure = false;
		if (p instanceof King && p.getLastMove() == -1) {
//			System.out.println("islegalmove(), castling?");
//			System.out.println("into check for castling");

			char selfSide = p.getSide();
			char oppoSide;
			if (selfSide == 'w') {
				oppoSide = 'b';
				// case 1:	king moves to the left
				if (p.getFile()-newF == 2 && p.getRank() == newR && pieces[0][0].getLastMove()==-1) {
					if (board[p.getRank()][1]==null && board[p.getRank()][2]==null
						&& board[p.getRank()][3]==null) {
						// check if being checked
						for (int i =0; i<16 ; i++) {
							if ((pieces[1][i].isValid(p.getRank(), p.getFile(), board) ||
									pieces[1][i].isValid(0,3, board)||
									pieces[1][i].isValid(0,2, board))&& !pieces[1][i].isCaptured()) {
								legal = -1;
								castleFailure= true;
								break;
							}
							legal = 2;
							
						}
					}
				}
				// case 2: king moves to the right
				if (newF-p.getFile()==2 && p.getRank() == newR && pieces[0][7].getLastMove()==-1) {
					if (board[p.getRank()][5]==null && board[p.getRank()][6]==null ) {
						// check if being checked
						for (int i =0; i<16 ; i++) {
							if ((pieces[1][i].isValid(p.getRank(), p.getFile(), board)||
									pieces[1][i].isValid(0,5, board)||
									pieces[1][i].isValid(0,6, board))&&!pieces[1][i].isCaptured()) {
								legal = -1;
								castleFailure= true;
								break;
							}
							legal = 2;
						}
					}
				}
			}else{
				oppoSide = 'w';
				// case 1:	king moves to the left
				if (p.getFile()-newF == 2 && p.getRank() == newR && pieces[1][0].getLastMove()==-1) {
					if (board[p.getRank()][1]==null && board[p.getRank()][2]==null
						&& board[p.getRank()][3]==null) {
						// check if being checked
						for (int i =0; i<16 ; i++) {
							if ((pieces[0][i].isValid(p.getRank(), p.getFile(), board)||
									pieces[0][i].isValid(7,2, board)||
									pieces[0][i].isValid(7,3, board))&& !pieces[0][i].isCaptured()) {
								legal = -1;
								castleFailure= true;
								break;
							}
							legal = 2;
						}
					}
				}
				// case 2: king moves to the right
				if (newF-p.getFile()==2 && p.getRank() == newR && pieces[1][7].getLastMove()==-1) {
					if (board[p.getRank()][5]==null && board[p.getRank()][6]==null ) {
						// check if being checked
						for (int i =0; i<16 ; i++) {
							if ((pieces[0][i].isValid(p.getRank(), p.getFile(), board)||
									pieces[0][i].isValid(7,5, board)||
									pieces[0][i].isValid(7,6, board)) && !pieces[0][i].isCaptured()) {
								
//								System.out.println("try");
								legal = -1;
								castleFailure= true;
								break;
							}
							legal = 2;
						}
					}
				}

			}
//			System.out.println("Castling legal number"+legal);
		}

//		System.out.println("isLegalMove(), check?");
		// if after update, opponent has pieces that can validly move to our king 
		int pTempFile = p.getFile();
		int pTempRank = p.getRank();

		int selfSide = (p.getSide() == 'w' ? 0:1);
		int oppoSide = (selfSide == 0? 1:0);
		int self, oppo;
		boolean capture = false;

		
		// temp capture
		for (oppo = 0; oppo < 16; oppo++){
			int fl = pieces[oppoSide][oppo].getFile();
			int rk = pieces[oppoSide][oppo].getRank();

			int captureRank = (legal == 1? (selfSide == 0? 4:3):newR);

			if (fl == newF && rk == captureRank && !pieces[oppoSide][oppo].isCaptured()) {
				pieces[oppoSide][oppo].beCaptured();
				capture = true;
				break;
			}
		}

		// temp move
		for ( self = 0; self < 16; self++){
			int fl = pieces[selfSide][self].getFile();
			int rk = pieces[selfSide][self].getRank();

			if (fl == pTempFile && rk == pTempRank && !pieces[selfSide][self].isCaptured()) {
				pieces[selfSide][self].tmpMove(newR, newF);
				break;
			}
		}

		// update board
		update();

		// traverse oppo side piece
		int count = 0 ;
		for (int i = 0; i<16; i++) {
			if (pieces[oppoSide][i].isValid(pieces[selfSide][4].getRank(), pieces[selfSide][4].getFile(), board) &&
					!pieces[oppoSide][i].isCaptured()) {
//				System.out.println(" debug: in check !");
				count++;
				legal = -1;
			}
		}
		
//		System.out.println("count:"+count+"\tlegal:"+legal);
		
		if (count == 0 && legal == -1 && obliqMove && !castleFailure) {
			legal = 0;
//			System.out.println("legal!!!!");
		}	
		
		// temp move redo
		pieces[selfSide][self].tmpMove(pTempRank, pTempFile);
		if (capture)
			pieces[oppoSide][oppo].captured = false;

		update();
		return legal;
	}

	/**
	 * method to print the board
	 */
	public void printBoard(){
		
		for (int rk = 7; rk > -1; rk--)
		{
			for (int fl = 0; fl < 8; fl++)
			{
				if (board[rk][fl] == null)
				{
					if ((rk+fl)%2 == 0)
						System.out.print("##");
					else
						System.out.print("  ");
				}
				else
					System.out.print(board[rk][fl]);

				System.out.print(" ");
			}
			// print rank number
			System.out.print(rk+1 +"\n");
		}

		// print file char
		// a  b  c  d  e  f  g  h
		for (char flc = 97; flc < 105; flc ++)
			System.out.print(" "+flc+" ");

		System.out.println("\n");
	}


}
