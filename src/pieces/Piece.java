package pieces;

/**
 * @author	Chenjie Wu, Chuanqi Xiong
 *
 */
public abstract class Piece {
	
	
	int file;
	int rank;
	char side;
	String name = null;
	private int lastMove;	// last move of pieces in chess game, -1 if never moved
	public boolean captured;
	
	/**
	 * @param rank	instance current rank
	 * @param file	instance current file
	 * @param side	instance current side
	 */
	public Piece(int rank, int file, char side) {
		this.file = file;
		this.rank = rank;
		this.lastMove = -1;
		this.side = side;
		this.captured = false;
	}

	/**
	 * @param newR	intention move's rank
	 * @param newF	intention move's file
	 * @return true if the intention move is logically correct
	 */
	public abstract boolean isPattern(int newR, int newF);

	/**
	 * @param newR	intention move's rank
	 * @param newF	intention move's file
	 * @return true if the intention move is blocked
 	 */
	public boolean isBlocked(int newR, int newF, Piece[][] board){
		boolean block = false;

//		System.out.println("welcome to isblocked()");
		
//		// case: the intention move is not in pattern
//		if (!isPattern(newR, newF))
//		{
//			System.out.println("isblocked():not in pattern"); 
//			return true;
//		}

		// case: same position as current
		if (newF == this.file && newR == this.rank) {
//			 System.out.println("intention move is its current location");
			return true;
		}

		// check if pattern is straight line
		// case1: file is the same
		else if (newF == this.file) {
			if (newR > this.rank) {
				for (int i = this.rank + 1; i < newR; i++ ) {
					if (board[i][newF] != null) {
//						 System.out.println("the block piece is " + board[i][newF]);
						block = true;
					}
				}
			}
			if (newR < this.rank) {
				for (int i = newR + 1; i < this.rank; i++ ) {
					if (board[i][newF] != null) {
//						 System.out.println("the block piece is " + board[i][newF]);
						block = true;
					}
				}
			}
		}
		// case 2:	rank is the same
		else if (newR == this.rank) {
			if (newF > this.file) {
				for (int i = this.file + 1; i < newF; i++ ) {
					if (board[newR][i] != null) {
//						 System.out.println("the block piece is " + board[newR][i]);
						block = true;
					}
				}
			}
			if (newF < this.file) {
				for (int i = newF + 1; i < this.file; i++ ) {
					if (board[newR][i] != null) {
//						 System.out.println("the block piece is " + board[newR][i]);
						block = true;
					}
				}
			}
		}

		// check if pattern is oblique line
		// case 1:	this.file < newF && this.rank < newR
		else if (this.file < newF && this.rank < newR)
		{
			for (int i =  1; i < newF - this.file; i++)
			{
				if(board[newR-i][newF-i] != null)
				{
//					 System.out.println("the block piece is "+ board[newR-i][newF-i]);
					block = true;
				}
			}
		}
		// case 2:	this.file>newF && this.rank<newR
		else if (this.file > newF && this.rank < newR)
		{
			for (int i = 1; i < this.file- newF; i++ )
			{
				if(board[newR-i][newF+i]!=null)
				{
//					 System.out.println("the block piece is "+ board[newR-i][newF+i]);
					block = true;
				}
			}
		}

		// case 3:	this.file<newF && this.rank>newR
		else if (this.file < newF && this.rank > newR)
		{
			for (int i = 1; i < newF - this.file; i++ )
			{
				if (board[newR+i][newF-i] != null)
				{
//					 System.out.println("the block piece is "+ board[newR+i][newF-i] );
					block = true;
				}
			}
		}
		// case 4:	this.file>newF && this.rank>newR
		else if (this.file > newF && this.rank > newR)
		{
			for (int i = 1; i < this.file - newF; i++)
			{
				if(board[newR+i][newF+i] != null)
				{
//					 System.out.println("the block piece is "+ board[newR+i][newF+i]);
					block = true;
				}
			}
		}
		else 
			System.out.println("not blocked");

		
		return block;
	}

	/**
	 * @param newR	intention move's rank
	 * @param newF	intention move's file
	 * @param board	current board
	 * @return	true if a intended move/capture is valid
	 */
	public boolean isValid(int newR, int newF, Piece[][] board)
	{
		if (isPattern(newR,newF))
		{
			if (!isBlocked(newR,newF,board))
			{
				// piece move to an empty cell
				if (board[newR][newF] == null)
					return true;

				// piece capture
				else if (board[newR][newF].getSide() != this.side)
					return true;

//				System.out.println("-"+this.name+"- bad pattern1");
			}
//			else 
//				System.out.println("-"+this.name+"- bad pattern2");
		}
//		else 
//			System.out.println("-"+this.name+"- not in pattern");
		
		return false;
	}

	/**
	 * @param newR	intention move's rank
	 * @param newF	intention move's file
	 * @param movetoken	turn's recording token
	 */
	public void move(int newR, int newF, int movetoken) {
		this.file = newF;
		this.rank = newR;
		this.lastMove = movetoken;
	}

	/**
	 * temporary move to a new position (for legalMove check)
	 * @param newR	intention move's rank
	 * @param newF	intention move's file
	 */
	public void tmpMove(int newR, int newF){
		this.file = newF;
		this.rank = newR;
	}

	/**
	 * change the new created object of pawn to current token
	 * @param newToken current token
	 */
	public void promToken(int newToken) {
		this.lastMove = newToken;
	}
	
	/**
	 * @return the token which this piece moved
	 */
	public int getLastMove(){
		return this.lastMove;
	}

	/**
	 * @return 'w' if is white piece, 'b' otherwise
	 */
	public char getSide() {
		return this.side;
	}

	/**
	 * @return file of this piece
	 */
	public int getFile() {
		return this.file;
	}

	/**
	 * @return rank of this piece
	 */
	public int getRank() {
		return this.rank;
	}

	/**
	 * set this piece to be captured
	 */
	public void beCaptured(){
		this.captured = true;
	}

	/**
	 * @return	true if this piece is captured
	 * 			false if not captured
	 */
	public boolean isCaptured(){
		return this.captured;
	}

	@Override
	public String toString() {
		return this.name;
	}



	
}
