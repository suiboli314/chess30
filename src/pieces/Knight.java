package pieces;

/**
 * @author Chenjie Wu, Chuanqi Xiong
 *
 */
public class Knight extends Piece {

	/**
	 * initiate Knight piece
	 * @param rank	instance current rank
	 * @param file	instance current file
	 * @param side composes as a part of name, after with an Uppercase N standing for bishop. eg. wN
	 */
	public Knight(int rank, int file, char side) {
		super(rank, file, side);
		this.name = side + "N";
	}

	/* (non-Javadoc)
	 * @see pieces.Piece#isPattern(int, int)
	 */
	@Override
	public boolean isPattern(int newR, int newF)
	{
		// xF, xR are the differences between this.file and newFile, this.rank and newRank
		int xF,xR;
		xF = Math.abs(newF - this.file);
		xR = Math.abs(newR - this.rank);

		if ((xF == 2 && xR == 1) || (xF == 1 && xR == 2))
			return true;

		return false;
	}

	/* (non-Javadoc)
	 * @see pieces.Piece#isValid(int, int, pieces.Piece[][])
	 */
	@Override
	public boolean isValid(int newR, int newF, Piece[][] board)
	{
		if (isPattern(newR,newF))
		{
			// Knight will never be blocked
			// piece move to an empty cell
			if (board[newR][newF] == null)
				return true;

			// piece capture
			else if (board[newR][newF].getSide() != this.side)
				return true;
			
//			System.out.println("-"+this.name+"-bad pattern");
		}
//		else
//			System.out.println("-"+this.name+"-not in pattern");
		
		return false;
	}




}
