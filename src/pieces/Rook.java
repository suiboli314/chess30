package pieces;

/**
 * @author Chenjie Wu, Chuanqi Xiong
 *
 */
public class Rook extends Piece
{
	/**
	 * initiate Rook piece
	 * @param rank	instance current rank
	 * @param file	instance current file
	 * @param side composes as a part of name, after with an Uppercase R standing for rook. eg. wR
	 */
	public Rook(int rank, int file, char side)
	{
		super(rank, file, side);
		this.name = side + "R";
	}

	/* (non-Javadoc)
	 * @see pieces.Piece#isPattern(int, int)
	 */
	@Override
	public boolean isPattern(int newR, int newF) {

		if(this.file != newF && this.rank == newR)
			return true;
		
		if(this.file == newF && this.rank != newR)
			return true;

		return false;
	}
	

}
