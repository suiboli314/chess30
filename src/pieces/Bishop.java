package pieces;

/**
 * @author Chenjie Wu, Chuanqi Xiong
 *
 */
public class Bishop extends Piece {

	/**constructor
	 * @param rank	instance current rank
	 * @param file	instance current file
	 * @param side	composes as a part of name, after with an Uppercase B standing for king. eg. wB
	 */
	public Bishop(int rank, int file, char side)
	{
		super(rank, file, side);
		this.name = side + "B";
	}

	/* (non-Javadoc)
	 * @see pieces.Piece#isPattern(int, int)
	 */
	@Override
	public boolean isPattern(int newR, int newF)
	{
		// xF, xR are the differences between this.file and newFile, this.rank and newRank
		int xF,xR;
		xF = Math.abs(newF - this.file);
		xR = Math.abs(newR - this.rank);

		if (xF == xR)
			return true;

		return false;
	}
	


}
