package pieces;

/**
 * @author Chenjie Wu, Chuanqi Xiong
 *
 */
public class Queen extends Piece {

	/**
	 * initiate Queen piece
	 * @param rank	instance current rank
	 * @param file	instance current file
	 * @param side composes as a part of name, after with an Uppercase Q standing for queen. eg. wQ
	 */
	public Queen(int rank, int file, char side)
	{
		super(rank, file, side);
		this.name = side + "Q";
	}

	/* (non-Javadoc)
	 * @see pieces.Piece#isPattern(int, int)
	 */
	@Override
	public boolean isPattern(int newR, int newF)
	{
		// xF, xR are the differences between this.file and newFile, this.rank and newRank
		int xF,xR;
		xF = Math.abs(newF - this.file);
		xR = Math.abs(newR - this.rank);

		// case1: move within the same rank
		if(this.file != newF && this.rank == newR)
			return true;

		// case2: move within the same file
		if(this.file == newF && this.rank != newR)
			return true;

		// case3: move within the same differences of newF and newR.
		if((xF == xR) && (xF != 0))
			return true;

		// case4: invalid move
		return false;
	}
	


}
