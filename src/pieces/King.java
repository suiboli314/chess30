package pieces;

/**
 * @author Chenjie Wu, Chuanqi Xiong
 *
 */
public class King extends Piece
{
	/**
	 * initiate King piece
	 * @param rank	instance current rank
	 * @param file	instance current file
	 * @param side composes as a part of name, after with an Uppercase K standing for king. eg. wK
	 */
	public King(int rank, int file, char side)
	{
		super(rank, file, side);
		this.name = side + "K";
	}

	/* (non-Javadoc)
	 * @see pieces.Piece#isPattern(int, int)
	 */
	@Override
	public boolean isPattern(int newR, int newF)
	{
		// case1: casteling
		int sideRank = (this.side == 'w'? 0: 7);

		// king is at his side, and never moved
		if (this.rank == sideRank && this.getLastMove() == -1) {
			//king is at initial slot, file == 'e'
			if (this.file == 4) {
				if (newF == 2 || newF == 6)
					return true;
			}
		}
		
		int xF, xR;
		xF = Math.abs(newF - this.file);
		xR = Math.abs(newR - this.rank);
		
		if (xR == 0 && xF == 0)
			return false;
			
		if (xR <= 1 && xF <= 1)
			return true;

		return false;
	}

	/* (non-Javadoc)
	 * @see pieces.Piece#isValid(int, int, pieces.Piece[][])
	 */
	@Override
	public boolean isValid(int newR, int newF, Piece[][] board)
	{
		if (isPattern(newR,newF))
		{
			// king castling
			int rookFile = (newF == 2? 0:7);
			int xF = Math.abs(newF - this.file);
			int xR = Math.abs(newR - this.rank);

			if (xF == 2 && this.rank == newR )
			{
				if (board[newR][rookFile] != null && !isBlocked(this.rank,rookFile,board))
				{
					if (board[newR][rookFile].toString().contains("R"))
					{
						return true;
					}
				}
			}	
				
			// king normal move/capture
			else if (xF <= 1 && xR <= 1)
			{
				// king move to an empty cell
				if (board[newR][newF] == null)
					return true;
	
				// king capture
				else if (board[newR][newF].getSide() != this.side)
					return true;
			}
			

		}


		return false;
	}



}
