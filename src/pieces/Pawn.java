package pieces;

/**
 * @author Chenjie Wu, Chuanqi Xiong
 *
 */
public class Pawn extends Piece
{
	/**
	 * initiate pawn piece
	 * @param rank	instance current rank
	 * @param file	instance current file
	 * @param side composes as a part of name, after with an Lowercase p standing for pawn. eg. wp
	 */
	public Pawn(int rank, int file, char side) {
		super(rank, file, side);
		this.name = side + "p";
	}

	/* (non-Javadoc)
	 * @see pieces.Piece#isPattern(int, int)
	 */
	@Override
	public boolean isPattern(int newR, int newF)
	{
		boolean isPatt = false;

		// case1: move backward
		if (this.side == 'w')
		{
			if(newR < this.rank)
			{
//				System.out.println("this.rk="+this.rank+"\tnewR"+newR);
//				System.out.println("-"+this.name+"-move back");
				return isPatt;
			}
		}
		else
		{
			if(newR > this.rank)
			{
//				System.out.println("-"+this.name+"-move back");
				return isPatt;
			}
		}

		// xF, xR are the differences between this.file and newFile, this.rank and newRank
		int xF, xR;
		xF = Math.abs(newF - this.file);
		xR = Math.abs(newR - this.rank);

		// case1: move forward 1
		if (xR == 1 && xF == 0)
			isPatt = true;

		// case2: capture!!! override isValid require
		else if (xF == 1 && xR == 1)
			isPatt = true;

		// case3: move forward 2, if never moved
		else if (this.getLastMove() == -1 && xR == 2 && xF == 0 )
			isPatt = true;
		
		
		return isPatt;
	}

	/* (non-Javadoc)
	 * @see pieces.Piece#isValid(int, int, pieces.Piece[][])
	 */
	@Override
	public boolean isValid(int newR, int newF, Piece[][] board)
	{
		int xF;
		xF = Math.abs(newF - this.file);

		if (isPattern(newR,newF))
		{
			// pawn moves forward
			if (board[newR][newF] == null && xF == 0)
			{
				if (!isBlocked(newR, newF, board))
					return true;
				
//				System.out.println("-"+this.name+"-move blocked");
			}

			// pawn captures
			else if (board[newR][newF] != null && xF == 1)
			{
				// opposite pieces
				if (board[newR][newF].getSide() != this.side)
					return true;
				
//				System.out.println("-"+this.name+"-selfside capture invalid");
			}
//			enpassant case
			else if (board[newR][newF] == null 
					&& xF == 1
					&& this.rank == (this.getSide() == 'w'? 3:4)
					&& board[this.rank][newF] != null) 
			{
				if (board[this.rank][newF] instanceof Pawn)
					return true;
			}
//				System.out.println("-"+this.name+"-bad pattern");
		}
//		else
//			System.out.println("-"+this.name+"-not in pattern");
		return false;
	}



}
